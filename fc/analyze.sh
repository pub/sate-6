#!/bin/bash -eu

# For performance reasons, this script retrieves most information from
# environment variables passed to it.

if [ -z "${FRAMAC-}" ]; then
    echo "error: FRAMAC environment variable empty or undefined"
    exit 1
fi
if [ -z "${FRAMAC_SHARE-}" ]; then
    echo "error: FRAMAC_SHARE environment variable empty or undefined"
    exit 1
fi
if [ -z "${TESTCASESUPPORT_DIR-}" ]; then
    echo "error: TESTCASESUPPORT_DIR environment variable empty or undefined"
    exit 1
fi
if [ -z "${SCRIPT_DIR-}" ]; then
    echo "error: SCRIPT_DIR environment variable empty or undefined"
    exit 1
fi
if [ ! -z "${GUI-}" ]; then
    use_gui=1
    CMD_DIR="$(dirname ${FRAMAC})"
    if [ "$CMD_DIR" = "." ]; then
        FRAMAC="${GUI}"
    else
        FRAMAC="${CMD_DIR}/${GUI}"
    fi
else
    use_gui=0
fi

CPPFLAGS="\
  -I$TESTCASESUPPORT_DIR \
  -nostdinc \
  -I$FRAMAC_SHARE/libc \
"

# disable unused plug-ins to speed up analysis
FCFLAGS="-no-autoload-plugins -load-module from,inout,report,eva,variadic -kernel-warn-key parser:decimal-float=inactive -kernel-warn-key typing:no-proto=inactive -kernel-warn-key typing:implicit-conv-void-ptr=inactive -eva-warn-key locals-escaping=inactive -add-symbolic-path $TESTCASESUPPORT_DIR:TESTCASESUPPORT_DIR -machdep x86_32"

EVAFLAGS="\
  -eva-msg-key=-initial-state,-final-states \
  -eva-no-show-progress \
  -eva-print-callstacks \
  -eva-slevel 300 \
  -warn-special-float none \
  -warn-signed-downcast \
  -warn-unsigned-overflow \
  -eva-warn-copy-indeterminate=-@all \
  -eva-no-remove-redundant-alarms \
  -eva-domains equality,sign \
  -eva-verbose 0 \
"

if [ $use_gui -eq 1 ]; then
    TIMEOUT=""
    # Add other plugins that may be useful in the GUI
    FCFLAGS+=" -load-module dive,metrics,studia"
else
    TIMEOUT="timeout 30s "
fi

# detect if we are being run via make, with flag VERBOSE
set +e
printenv MAKEFLAGS | grep -q '\bVERBOSE='
RES=$?
set -e
if [ $RES -eq 0 ]; then
    set -x
fi
$TIMEOUT$FRAMAC -cpp-extra-args="$CPPFLAGS" \
                $FCFLAGS \
                -eva $EVAFLAGS \
                $SCRIPT_DIR/fc_runtime.c \
                $TESTCASESUPPORT_DIR/io.c \
                "$@"
