#include "wchar.c"
#include "string.c"
// From "stdio.c"
FILE __fc_initial_stdout = {.__fc_FILE_id=1};
FILE * __fc_stdout = &__fc_initial_stdout;

FILE __fc_initial_stderr = {.__fc_FILE_id=2};
FILE * __fc_stderr = &__fc_initial_stderr;

FILE __fc_initial_stdin = {.__fc_FILE_id=0};
FILE * __fc_stdin = &__fc_initial_stdin;

#include "stdlib.h"

int abs (int i) {
  if (i < 0) return -i;
  return i;
}

long labs (long i) {
  if (i < 0L) return -i;
  return i;
}

long long llabs (long long i) {
  if (i < 0LL) return -i;
  return i;
}

// From "math.c"
double fabs(double x){
  if(x==0.0) return 0.0;
  if (x>0.0) return x;
  return -x;
}

float fabsf(float x)
{
  if (x == 0.0f) {
    return 0.0f;
  } else if (x > 0.0f) {
    return x;
  } else {
    return -x;
  }
}
