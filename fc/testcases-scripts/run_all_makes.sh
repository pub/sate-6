#!/bin/bash -eu

# to ensure printf uses '.' instead of ',' for decimal separator
export LC_ALL=C

exit_on_signal() {
    echo "sigint received, aborting."
    rm -f running_make_all.txt
    exit 1
}
trap exit_on_signal INT


dirs=($(find CWE* -name GNUmakefile | sed s/GNUmakefile//))
touch running_make_all.txt
(parallel -j 4 --linebuffer --verbose 'cd {} && make -j 4 -k' ::: ${dirs[@]} | tee output.txt; rm -f running_make_all.txt) &

total=46330 # counted manually
count=0
while [ -f running_make_all.txt ]; do
    sleep 5
    count=$((count+5))
    processed=$(grep testing output.txt | wc -l)
    avg=$(echo "$processed / $count" | bc -l)
    remaining=$(echo "$total - $processed" | bc)
    eta=$(echo "$remaining / $avg" | bc -l)
    pp_avg=$(printf "%.2f" $avg)
    pp_eta=$(printf "%.0f" $eta | dc -e '?60~r60~r[[0]P]szn[:]ndZ2>zn[:]ndZ2>zp')
    printf "\n***** PROCESSING (${count} s)... tested $processed files ($remaining remaining); average fps: $pp_avg; ETA: $pp_eta *****\n\n"
done
